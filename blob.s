	includeh	ls_ppc_macros.def
	includeh	memory.def
	includeh	quickdraw.def
	includeh	palettes.def
	includeh	resources.def
	
*equates
buffer_width:			equ	640
buffer_height:			equ	480
screen_width:			equ	640
screen_height:			equ 480
screen_size:			equ	screen_width*screen_height

squareroot_of_max_r:	equ	160
max_r:					equ squareroot_of_max_r*squareroot_of_max_r

*bss
bss:					reg		r30
buf0_handle:			rs.w	1
buf0_handle_orig:		rs.w	1

buf1_handle:			rs.w	1
buf1_handle_orig:		rs.w	1

	entry
	start_up
	bl	init_mac
	bl	init_graph_lib
		
	Xcall	HideCursor

set_clut:
	li		r4,129
	
	li		r3,"cl"
	slwi	r3,r3,16
	li		r5,"ut"
	or		r3,r3,r5
	Xcall	GetResource	
	lwz		r5,(r3)

	li		r3,0
	li		r4,255
	Xcall	SetEntries
			
make_offscreen_buffer_0:
	movei	r3,buffer_width*buffer_height
	
	Xcall	NewHandle
	mr		r20,r3
	Xcall	HLock
	
	mr		r3,r20
	lwz		r3,(r3)
	
	subi	r3,r3,1

	stw		r3,buf0_handle(`bss)
	stw		r3,buf0_handle_orig(`bss)

make_offscreen_buffer_1:
	movei	r3,buffer_width*buffer_height
	Xcall	NewHandle
	mr		r20,r3
	Xcall	HLock
	
	mr		r3,r20
	lwz		r3,(r3)
	
	subi	r3,r3,1

	stw		r3,buf1_handle(`bss)
	stw		r3,buf1_handle_orig(`bss)

init_main_loop:
	lwz		r20,buf0_handle(`bss)
	
	li		r21,319
	li		r28,239
	
main_loop:
	bl		getkey_turbo
	lwz		r4,3(r4)
	cmpwi	r4,1
	beq		exit
			
	bl		getmouse
	
	lwz		r20,buf0_handle(`bss)
	movei	r22,screen_size
			
init_offscreen_buffer_loop:
	li		r10,0
	li		r11,0
	li		r12,$ff
	li		r13,max_r
	li		r17,0
	li		r18,0
	
offscreen_buffer_loop:
	addi	r17,r17,1	
	cmpwi	r17,640
	bne		no_new_line	
	
	li		r17,0
	addi	r18,r18,1
no_new_line:

check_distance:
	sub		r11,r3,r17
	mullw	r11,r11,r11
			
	sub		r29,r4,r18
	mullw	r29,r29,r29
		
	add		r10,r29,r11
	
	divw	r10,r13,r10
	
	sub		r11,r21,r17 
	mullw	r11,r11,r11
	
	sub		r29,r28,r18
	mullw	r29,r29,r29
		
	add		r25,r29,r11
		
	divw	r25,r13,r25
		
	add		r10,r25,r10
	
	subi	r10,r10,$ff
							
	cmpwi	r10,$20
	bgt		draw_pixel
	bl		draw_blob_pixel

draw_pixel:		
	li		r19,$ff
	
	stbu	r19,1(r20)
		
	subi	r22,r22,1
	cmpwi	r22,0
	beq		init_blur

	bl		offscreen_buffer_loop
	
draw_blob_pixel:	
	stbu	r10,1(r20)

	subi	r22,r22,1
	cmpwi	r22,0
	beq		init_blur
	
	bl		offscreen_buffer_loop

init_blur:
	movei	r22,screen_size

	lwz		r3,buf0_handle(`bss)
	lwz		r4,buf1_handle(`bss)
	
blur:
	lwz		r3,buf0_handle(`bss)
	
	lbz		r10,-641(r3)
	lbz		r11,-640(r3)
	lbz		r12,-639(r3)
	lbz		r13,-1(r3)
	lbz		r14,1(r3)
	lbz		r15,639(r3)
	lbz		r16,640(r3)
	lbz		r17,641(r3)
	
	add		r11,r11,r12
	add		r13,r13,r10
	add		r16,r16,r15
	add		r14,r14,r17
	
	add		r18,r13,r11
	add		r19,r14,r16
	
	lbz		r3,0(r3)
	
	add		r3,r19,r18

	stbu	r3,1(r4)
	
	lwz		r3,buf0_handle(`bss)
	addi	r3,r3,1	
	stw		r3,buf0_handle(`bss)
	
	subi	r22,r22,1
	
	cmpwi	r22,0
	bne		blur
	
	lwz		r3,buf0_handle_orig(`bss)
	stw		r3,buf0_handle(`bss)
	
blit:
	lwz		r15,buf0_handle(`bss)
	movei	r14,$8A008600
	movei	r13,$8A008600+768*480
	
draw_screen:
	lfd		f0,0(r15)
	stfd	f0,0(r14)
	lfd		f0,8(r15)
	stfd	f0,8(r14)
	lfd		f0,16(r15)
	stfd	f0,16(r14)
	lfd		f0,24(r15)
	stfd	f0,24(r14)
	lfd		f0,32(r15)
	stfd	f0,32(r14)
	lfd		f0,40(r15)
	stfd	f0,40(r14)
	lfd		f0,48(r15)
	stfd	f0,48(r14)
	lfd		f0,56(r15)
	stfd	f0,56(r14)
	lfd		f0,64(r15)
	stfd	f0,64(r14)
	lfd		f0,72(r15)
	stfd	f0,72(r14)
	lfd		f0,80(r15)
	stfd	f0,80(r14)
	lfd		f0,80(r15)
	stfd	f0,80(r14)
	lfd		f0,88(r15)
	stfd	f0,88(r14)
	lfd		f0,96(r15)
	stfd	f0,96(r14)
	lfd		f0,104(r15)
	stfd	f0,104(r14)
	lfd		f0,112(r15)
	stfd	f0,112(r14)
	lfd		f0,120(r15)
	stfd	f0,120(r14)
	lfd		f0,128(r15)
	stfd	f0,128(r14)
	lfd		f0,136(r15)
	stfd	f0,136(r14)
	lfd		f0,144(r15)
	stfd	f0,144(r14)
	lfd		f0,152(r15)
	stfd	f0,152(r14)
	lfd		f0,160(r15)
	stfd	f0,160(r14)
	lfd		f0,168(r15)
	stfd	f0,168(r14)
	lfd		f0,176(r15)
	stfd	f0,176(r14)
	lfd		f0,184(r15)
	stfd	f0,184(r14)
	lfd		f0,192(r15)
	stfd	f0,192(r14)
	lfd		f0,200(r15)
	stfd	f0,200(r14)
	lfd		f0,208(r15)
	stfd	f0,208(r14)
	lfd		f0,216(r15)
	stfd	f0,216(r14)
	lfd		f0,224(r15)
	stfd	f0,224(r14)
	lfd		f0,232(r15)
	stfd	f0,232(r14)
	lfd		f0,240(r15)
	stfd	f0,240(r14)
	lfd		f0,248(r15)
	stfd	f0,248(r14)
	lfd		f0,256(r15)
	stfd	f0,256(r14)
	lfd		f0,264(r15)
	stfd	f0,264(r14)
	lfd		f0,272(r15)
	stfd	f0,272(r14)
	lfd		f0,280(r15)
	stfd	f0,280(r14)
	lfd		f0,288(r15)
	stfd	f0,288(r14)
	lfd		f0,296(r15)
	stfd	f0,296(r14)
	lfd		f0,304(r15)
	stfd	f0,304(r14)
	lfd		f0,312(r15)
	stfd	f0,312(r14)
	lfd		f0,320(r15)
	stfd	f0,320(r14)
	lfd		f0,328(r15)
	stfd	f0,328(r14)
	lfd		f0,336(r15)
	stfd	f0,336(r14)
	lfd		f0,344(r15)
	stfd	f0,344(r14)
	lfd		f0,352(r15)
	stfd	f0,352(r14)
	lfd		f0,360(r15)
	stfd	f0,360(r14)
	lfd		f0,368(r15)
	stfd	f0,368(r14)
	lfd		f0,376(r15)
	stfd	f0,376(r14)
	lfd		f0,384(r15)
	stfd	f0,384(r14)
	lfd		f0,392(r15)
	stfd	f0,392(r14)
	lfd		f0,400(r15)
	stfd	f0,400(r14)
	lfd		f0,408(r15)
	stfd	f0,408(r14)
	lfd		f0,416(r15)
	stfd	f0,416(r14)
	lfd		f0,424(r15)
	stfd	f0,424(r14)
	lfd		f0,432(r15)
	stfd	f0,432(r14)
	lfd		f0,440(r15)
	stfd	f0,440(r14)
	lfd		f0,448(r15)
	stfd	f0,448(r14)
	lfd		f0,456(r15)
	stfd	f0,456(r14)
	lfd		f0,464(r15)
	stfd	f0,464(r14)
	lfd		f0,472(r15)
	stfd	f0,472(r14)
	lfd		f0,480(r15)
	stfd	f0,480(r14)
	lfd		f0,488(r15)
	stfd	f0,488(r14)
	lfd		f0,496(r15)
	stfd	f0,496(r14)
	lfd		f0,504(r15)
	stfd	f0,504(r14)
	lfd		f0,512(r15)
	stfd	f0,512(r14)
	lfd		f0,520(r15)
	stfd	f0,520(r14)
	lfd		f0,528(r15)
	stfd	f0,528(r14)
	lfd		f0,536(r15)
	stfd	f0,536(r14)
	lfd		f0,544(r15)
	stfd	f0,544(r14)
	lfd		f0,552(r15)
	stfd	f0,552(r14)
	lfd		f0,560(r15)
	stfd	f0,560(r14)
	lfd		f0,568(r15)
	stfd	f0,568(r14)
	lfd		f0,576(r15)
	stfd	f0,576(r14)
	lfd		f0,584(r15)
	stfd	f0,584(r14)
	lfd		f0,592(r15)
	stfd	f0,592(r14)
	lfd		f0,600(r15)
	stfd	f0,600(r14)
	lfd		f0,608(r15)
	stfd	f0,608(r14)
	lfd		f0,616(r15)
	stfd	f0,616(r14)
	lfd		f0,624(r15)
	stfd	f0,624(r14)
	lfd		f0,632(r15)
	stfd	f0,632(r14)
	
	addi	r15,r15,640
	addi	r14,r14,768
	
	cmpw	r14,r13
	bne		draw_screen
	
	bl		main_loop
	
exit:
	tidy_up
	blr

	extern	init_mac,init_graph_lib,getmouse,getkey_turbo